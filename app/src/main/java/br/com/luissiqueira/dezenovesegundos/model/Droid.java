package br.com.luissiqueira.dezenovesegundos.model;

/**
 * Created by luissiqueira on 09/04/14.
 */

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;

/**
 * This is a test droid that is dragged, dropped, moved, smashed against
 * the wall and done other terrible things with.
 * Wait till it gets a weapon!
 *
 * @author impaler
 *
 */
public class Droid {

    private Bitmap bitmap;	// the actual bitmap
    private int x;			// the X coordinate
    private int y;			// the Y coordinate
    private boolean touched;	// if droid is touched/picked up
    private Speed speed;	// the speed with its directions

    public Droid(Bitmap bitmap, int x, int y) {
        this.bitmap = bitmap;
        this.x = x;
        this.y = y;
        this.speed = new Speed();
    }

    public Droid(boolean main, int x, int y) {
        this.bitmap = Bitmap.createBitmap(80, 80, Bitmap.Config.ARGB_8888);
        if(main)
            this.bitmap.eraseColor(Color.argb(255, 255, 0, 0));
        else
            this.bitmap.eraseColor(Color.argb(255, 0, 0, 255));
        this.x = x;
        this.y = y;
        this.speed = new Speed();
    }

    public Droid(boolean main, int x, int y, int px, int py) {
        this.bitmap = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
        if(main)
            this.bitmap.eraseColor(Color.argb(255, 152, 0, 0));
        else
            this.bitmap.eraseColor(Color.argb(255, 0, 0, 152));
        this.x = px;
        this.y = py;
        this.speed = new Speed();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }

    public Speed getSpeed() {
        return speed;
    }

    public void setSpeed(Speed speed) {
        this.speed = speed;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(bitmap, x - (bitmap.getWidth() / 2), y - (bitmap.getHeight() / 2), null);
    }

    /**
     * Method which updates the droid's internal state every tick
     */
    public void update() {
        if (!touched) {
            x += (speed.getXv() * speed.getxDirection());
            y += (speed.getYv() * speed.getyDirection());
        }
    }


    /**
     * Handles the {@link android.view.MotionEvent.ACTION_DOWN} event. If the event happens on the
     * bitmap surface then the touched state is set to <code>true</code> otherwise to <code>false</code>
     * @param eventX - the event's X coordinate
     * @param eventY - the event's Y coordinate
     */
    public boolean handleActionDown(int eventX, int eventY) {
        if (eventX >= (x - bitmap.getWidth() / 2) && (eventX <= (x + bitmap.getWidth()/2))) {
            if (eventY >= (y - bitmap.getHeight() / 2) && (eventY <= (y + bitmap.getHeight() / 2))) {
                // droid touched
                setTouched(true);
                return true;
            } else {
                setTouched(false);
            }
        } else {
            setTouched(false);
        }
        return false;

    }
}