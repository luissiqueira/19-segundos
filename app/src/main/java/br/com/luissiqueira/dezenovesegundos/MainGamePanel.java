/**
 *
 */
package br.com.luissiqueira.dezenovesegundos;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.luissiqueira.dezenovesegundos.model.Droid;
import br.com.luissiqueira.dezenovesegundos.model.Jogada;
import br.com.luissiqueira.dezenovesegundos.model.Speed;
import br.com.luissiqueira.dezenovesegundos.sqlite.MySQLiteHelper;

public class MainGamePanel extends SurfaceView implements
        SurfaceHolder.Callback {

    private Display display;
    public MainThread thread;
    private List<Droid> obstaculos = new ArrayList<Droid>(){};
    private Droid droid;
    private boolean loaded = false;
    private boolean running = false;
    private boolean finished = false;
    private static final long SECOND_TO_NANO = 1000000000L;
    private static final double NANO_TO_SECOND = 1. / SECOND_TO_NANO;
    public static float BASE;

    long startGame, stopGame;
    double totalGame, recorde;
    private MySQLiteHelper mDB;

    private int LIMITE_ESQUERDO,LIMITE_SUPERIOR, LIMITE_DIREITO, LIMITE_INFERIOR;

    public MainGamePanel(Context context) {
        super(context);
        getHolder().addCallback(this);
        display = ((Activity)context).getWindowManager().getDefaultDisplay();
        SharedPreferences sharedPref = ((Activity)context).getPreferences(Context.MODE_PRIVATE);
        recorde = (double) sharedPref.getFloat("recorde",(float)0.0);
        mDB = new MySQLiteHelper(context);
        thread = new MainThread(getHolder(), this);

        if(display.getWidth() <= display.getHeight()){
            LIMITE_ESQUERDO = 0;
            LIMITE_SUPERIOR = (display.getHeight()/2) - (display.getWidth()/2);
            LIMITE_DIREITO = display.getWidth();
            LIMITE_INFERIOR = (display.getHeight()/2) + (display.getWidth()/2);
            BASE = (float) (LIMITE_DIREITO / 480.0);
        }else{
            LIMITE_ESQUERDO = (display.getWidth()/2) - (display.getHeight()/2);
            LIMITE_SUPERIOR = 0;
            LIMITE_DIREITO = (display.getWidth()/2) + (display.getHeight()/2);
            LIMITE_INFERIOR = display.getHeight();
            BASE = (float) (LIMITE_INFERIOR / 800.0);
        }



        initGame();

        //setFocusable(true);
    }

    public float dpToPixel(float dp, Context context){
        return (dp * (getContext().getResources().getDisplayMetrics().densityDpi / 160f));
    }

    public void initGame(){

        int largura_droid = (int)(LIMITE_DIREITO * 0.14);
        int altura_droid = (int)(LIMITE_DIREITO * 0.14);
        droid = new Droid(true, largura_droid, altura_droid, display.getWidth()/2, display.getHeight()/2);

        obstaculos = new ArrayList<Droid>(){};

        largura_droid = (int)(LIMITE_DIREITO * 0.17);
        altura_droid = (int)(LIMITE_DIREITO * 0.17);
        obstaculos.add(new Droid(false, largura_droid, altura_droid,
                LIMITE_ESQUERDO + largura_droid,
                LIMITE_SUPERIOR + altura_droid));

        largura_droid = (int)(LIMITE_DIREITO * 0.2);
        altura_droid = (int)(LIMITE_DIREITO * 0.13);
        obstaculos.add(new Droid(false, largura_droid, altura_droid,
                (int) (LIMITE_DIREITO * 0.77),
                LIMITE_SUPERIOR + (int)(altura_droid * 1.3)));

        largura_droid = (int)(LIMITE_DIREITO * 0.08);
        altura_droid = (int)(LIMITE_DIREITO * 0.26);
        obstaculos.add(new Droid(false, largura_droid, altura_droid,
                LIMITE_ESQUERDO + (int) (largura_droid * 2),
                LIMITE_INFERIOR - altura_droid));

        largura_droid = (int)(LIMITE_DIREITO * 0.32);
        altura_droid = (int)(LIMITE_DIREITO * 0.08);
        obstaculos.add(new Droid(false, largura_droid, altura_droid,
                (int) (LIMITE_DIREITO * 0.7),
                LIMITE_INFERIOR - altura_droid ));


        loaded = true;
        running = false;
        finished = false;
        startGame = System.nanoTime();
    }

    public void finishGame(){
        loaded = false;
        running = false;
        finished = true;
        stopGame = System.nanoTime();
        totalGame = stopGame - startGame;
        totalGame = totalGame * NANO_TO_SECOND;
        String data = Calendar.getInstance().get(Calendar.DATE) + "/" + Calendar.getInstance().get(Calendar.MONTH);
        String tempo = new DecimalFormat("0.000").format(totalGame);
        mDB.addJogada(new Jogada(tempo.replace(",","."), data));
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        thread.setRunning(true);
        thread.setPriority(Thread.MAX_PRIORITY);
        try{
            thread.start();
        }
        catch (Exception e){
            thread = new MainThread(getHolder(), this);
            thread.setRunning(true);
            thread.setPriority(Thread.MAX_PRIORITY);
            thread.start();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {

            }
        }
    }

    public boolean inMenuArea(int x, int y){
        if (x > (LIMITE_DIREITO - (int) dpToPixel(100, getContext())) && x < LIMITE_DIREITO)
            if(y > ((int) dpToPixel(10, getContext())) && y < ((int) dpToPixel(50, getContext())))
                return true;

        return false;

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if(loaded && !running && droid.handleActionDown((int)event.getX(),(int)event.getY())){
                initGame();
                running = true;
            }else if(finished){
                initGame();
            }
            droid.handleActionDown((int)event.getX(),(int)event.getY());

            if(inMenuArea((int)event.getX(), (int)event.getY()))
                ((GameActivity)getContext()).openOptionsMenu();

        } if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (droid.isTouched() && running) {
                if(
                        (int)event.getX()-(droid.getBitmap().getWidth()/2) >= LIMITE_ESQUERDO &&
                                (int)event.getX()+(droid.getBitmap().getWidth()/2) < LIMITE_DIREITO){
                    droid.setX((int)event.getX());
                }
                if(
                        (int)event.getY()-(droid.getBitmap().getHeight()/2) >= LIMITE_SUPERIOR &&
                                (int)event.getY()+(droid.getBitmap().getHeight()/2)  < LIMITE_INFERIOR){
                    droid.setY((int)event.getY());
                }

            }
        } if (event.getAction() == MotionEvent.ACTION_UP) {
            if (droid.isTouched()) {
                droid.setTouched(false);
            }
        }
        return true;
    }

    public void render(Canvas canvas) {
        try{
            if(canvas!=null){
                canvas.drawColor(Color.BLACK);
                Paint p = new Paint();
                p.setColor(Color.rgb(238, 238, 238));
                canvas.drawRect(new Rect(LIMITE_ESQUERDO, LIMITE_SUPERIOR, LIMITE_DIREITO, LIMITE_INFERIOR), p);

                if(!finished){
                    for(Droid d:obstaculos)
                        d.draw(canvas);
                    droid.draw(canvas);
                }else{
                    Paint p1 = new Paint();
                    p1.setColor(Color.BLACK);
                    canvas.drawRect(new Rect((display.getWidth() / 2) - (int)dpToPixel((float)100.0, getContext()),
                            (display.getHeight() / 2) - (int)dpToPixel((float)40.0, getContext()),
                            (display.getWidth() / 2) + (int)dpToPixel((float)100.0, getContext()),
                            (display.getHeight() / 2) + (int)dpToPixel((float)40.0, getContext())), p1);
                    p1.setColor(Color.WHITE);
                    p1.setTextSize(dpToPixel((float)22.0, getContext()));
                    canvas.drawText(new DecimalFormat("0.000").format(totalGame) + " sec...",
                            (display.getWidth()/2) - (int)dpToPixel((float)55.0, getContext()),
                            (display.getHeight()/2) + (int)dpToPixel((float)8.0, getContext()), p1);
                    if(totalGame > recorde){
                        SharedPreferences sharedPref = ((Activity)getContext()).getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putFloat("recorde", (float) totalGame);
                        editor.commit();
                        recorde = totalGame;
                    }
                }

                Paint p2 = new Paint();
                p2.setColor(Color.WHITE);
                p2.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
                float px20 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());
                canvas.drawText("Recorde: ", px20, (float) (px20 * 2), p2);
                canvas.drawText(new DecimalFormat("0.000").format(recorde), (float) (px20 * 6.2), (float) (px20 * 2), p2);

                p2.setColor(Color.rgb(238, 238, 238));
                canvas.drawRect(new Rect(LIMITE_DIREITO - (int) dpToPixel(100, getContext()),
                        (int) dpToPixel(10, getContext()), LIMITE_DIREITO,
                        (int) dpToPixel(50, getContext())), p);
                p2.setTextSize(dpToPixel(20, getContext()));
                p2.setColor(Color.BLACK);
                canvas.drawText("Menu", LIMITE_DIREITO - (int)dpToPixel(75, getContext()),
                        (int)dpToPixel(37, getContext()), p2);

            }else if(GameActivity.isPaused){
                thread.setRunning(false);
            }else{
                thread.setRunning(false);
                ((Activity)getContext()).finish();
            }
        }catch (Exception e){e.printStackTrace();}
    }

    public void update() {


        if(running){

            for(Droid d : obstaculos){
                // check collision with right wall if heading right
                if (d.getSpeed().getxDirection() == Speed.DIRECTION_RIGHT
                        && d.getX() + d.getBitmap().getWidth() / 2 >= LIMITE_DIREITO) {
                    d.getSpeed().toggleXDirection();
                }
                // check collision with left wa\ll if heading left
                if (d.getSpeed().getxDirection() == Speed.DIRECTION_LEFT
                        && d.getX() - d.getBitmap().getWidth() / 2 <= LIMITE_ESQUERDO) {
                    d.getSpeed().toggleXDirection();
                }
                // check collision with bottom wall if heading down
                if (d.getSpeed().getyDirection() == Speed.DIRECTION_DOWN
                        && d.getY() + d.getBitmap().getHeight() / 2 >= LIMITE_INFERIOR) {
                    d.getSpeed().toggleYDirection();
                }
                // check collision with top wall if heading up
                if (d.getSpeed().getyDirection() == Speed.DIRECTION_UP
                        && d.getY() - d.getBitmap().getHeight() / 2 <= LIMITE_SUPERIOR) {
                    d.getSpeed().toggleYDirection();
                }

                int d_left = d.getX() - d.getBitmap().getWidth() / 2;
                int d_right = d.getX() + d.getBitmap().getWidth() / 2;
                int d_top = d.getY() - d.getBitmap().getHeight() / 2;
                int d_down = d.getY() + d.getBitmap().getHeight() / 2;

                int droid_left = droid.getX() - droid.getBitmap().getWidth() / 2;
                int droid_right = droid.getX() + droid.getBitmap().getWidth() / 2;
                int droid_top = droid.getY() - droid.getBitmap().getHeight() / 2;
                int droid_down = droid.getY() + droid.getBitmap().getHeight() / 2;

                Rect r_d = new Rect(d_left, d_top, d_right, d_down);
                Rect r_droid = new Rect(droid_left, droid_top, droid_right, droid_down);

                if(r_d.intersect(r_droid)){
                    finishGame();
                }

                d.update();
            }
        }
    }


}