package br.com.luissiqueira.dezenovesegundos.model;

import br.com.luissiqueira.dezenovesegundos.MainGamePanel;

/**
 * Created by luissiqueira on 09/04/14.
 */

public class Speed {

    public static final int DIRECTION_RIGHT	= 1;
    public static final int DIRECTION_LEFT	= -1;
    public static final int DIRECTION_UP	= -1;
    public static final int DIRECTION_DOWN	= 1;

    private float xv = 1;
    private float yv = 1;

    private int xDirection = Math.random() > 0.5 ? DIRECTION_RIGHT : DIRECTION_LEFT;
    private int yDirection = Math.random() > 0.5 ? DIRECTION_DOWN : DIRECTION_UP;

    public Speed() {
        this.xv = (3 + (int)(Math.random() * 4)) * MainGamePanel.BASE;
        this.yv = (3 + (int)(Math.random() * 3)) * MainGamePanel.BASE;

    }

    public float getXv() {
        return xv;
    }
    public float getYv() {
        return yv;
    }

    public int getxDirection() {
        return xDirection;
    }
    public int getyDirection() {
        return yDirection;
    }

    public void toggleXDirection() {
        xDirection = xDirection * -1;
    }

    public void toggleYDirection() {
        yDirection = yDirection * -1;
    }

}
