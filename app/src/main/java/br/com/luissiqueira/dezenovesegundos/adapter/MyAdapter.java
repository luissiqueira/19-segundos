package br.com.luissiqueira.dezenovesegundos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.luissiqueira.dezenovesegundos.R;
import br.com.luissiqueira.dezenovesegundos.model.Jogada;
import br.com.luissiqueira.dezenovesegundos.sqlite.MySQLiteHelper;

/**
 * Created by luissiqueira on 10/04/14.
 */
public class MyAdapter extends BaseAdapter {

    public static final int TYPE_ALL = 1, TYPE_BEST = 2;
    public List<Jogada> itens;
    private final Context context;
    public int type = TYPE_ALL;

    public MyAdapter(){
        itens = new ArrayList<Jogada>(){};
        context = null;
    }

    public MyAdapter(Context context){
        this.context = context;
        loadItens(TYPE_ALL);
    }

    public void loadItens(int t){
        MySQLiteHelper mDB = new MySQLiteHelper(context);
        this.itens = new ArrayList<Jogada>(){};
        switch (t){
            case TYPE_BEST:
                this.itens = mDB.getMelhoresJogadas();
            default:
                this.itens = mDB.getAllJogadas();
        }
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Jogada getItem(int i) {
        return itens.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        if(view == null)
            view = LayoutInflater.from(context).inflate(R.layout.jogada_historico, parent, false);

        TextView tv = (TextView) view.findViewById(R.id.textView);
        tv.setText(itens.get(i).getDia() + " - " + itens.get(i).getTempo());
        return view;
    }
}
