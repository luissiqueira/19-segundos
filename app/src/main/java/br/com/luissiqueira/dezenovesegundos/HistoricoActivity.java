package br.com.luissiqueira.dezenovesegundos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import br.com.luissiqueira.dezenovesegundos.adapter.MyAdapter;
import br.com.luissiqueira.dezenovesegundos.sqlite.MySQLiteHelper;

public class HistoricoActivity extends ActionBarActivity {

    MyAdapter mAd;
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_historico);
        mAd = new MyAdapter(getBaseContext());
        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(mAd);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getBaseContext(), GameActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(Menu.NONE, 1, Menu.NONE, getResources().getString(R.string.title_item_limpar));
        menu.add(Menu.NONE, 2, Menu.NONE, getResources().getString(R.string.title_item_recentes_primeiros));
        menu.add(Menu.NONE, 3, Menu.NONE, getResources().getString(R.string.title_item_melhores_primeiros));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        MySQLiteHelper mDB = new MySQLiteHelper(getBaseContext());

        switch (item.getItemId()){
            case 1:
                mDB.onUpgrade(mDB.getWritableDatabase(), 1, 1);
                mAd.loadItens(MyAdapter.TYPE_ALL);
                mAd.notifyDataSetChanged();
                return true;
            case 2:
                mAd.itens = mDB.getAllJogadas();
                mAd.notifyDataSetChanged();
                return true;
            case 3:
                mAd.itens = mDB.getMelhoresJogadas();
                mAd.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
