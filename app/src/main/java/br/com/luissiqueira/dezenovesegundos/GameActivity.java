package br.com.luissiqueira.dezenovesegundos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.RelativeLayout;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import java.text.DecimalFormat;

public class GameActivity extends Activity {

    private static final String AD_UNIT_ID = "a15345e579d0139";
    private AdView admobView;
    MainGamePanel mgp;
    public static boolean isPaused = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        admobView = new AdView(this, AdSize.BANNER, AD_UNIT_ID);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        admobView.setLayoutParams(lp);
        mgp = new MainGamePanel(this);
        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(mgp);

        layout.addView(admobView);
        admobView.loadAd(new AdRequest());

        setContentView(layout);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isPaused = false;
        if(mgp != null){
            mgp.thread.setRunning(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(Menu.NONE, 1, Menu.NONE, getResources().getString(R.string.title_item_historico));
        menu.add(Menu.NONE, 2, Menu.NONE, getResources().getString(R.string.title_item_compartilhar));
        menu.add(Menu.NONE, 3, Menu.NONE, getResources().getString(R.string.title_item_sair));
        return true;
    }

    public void showHistorico(){
        Intent i = new Intent(getBaseContext(), HistoricoActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 1:
                showHistorico();
                return true;
            case 2:
                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                double recorde = (double) sharedPref.getFloat("recorde",(float)0.0);
                String r = new DecimalFormat("0.000").format(recorde);
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Compartilhar recorde");
                i.putExtra(Intent.EXTRA_TEXT, "Meu atual recorde no 19 Segundos é de " + r + " segundos, e o seu? #19Segundos");
                startActivity(Intent.createChooser(i, "Compartilhar recorde"));
                return true;
            case 3:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}