package br.com.luissiqueira.dezenovesegundos.model;


/**
 * Created by luissiqueira on 10/04/14.
 */
public class Jogada {

    private String tempo;
    private String dia;
    private int id;

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Jogada() {}

    public Jogada(String tempo, String dia) {
        this.tempo = tempo;
        this.dia = dia;
    }
}
