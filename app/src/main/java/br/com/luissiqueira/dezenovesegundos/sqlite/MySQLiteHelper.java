package br.com.luissiqueira.dezenovesegundos.sqlite;

/**
 * Created by luissiqueira on 10/04/14.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import br.com.luissiqueira.dezenovesegundos.model.Jogada;

public class MySQLiteHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "JogadasDB";

    // Books table name
    private static final String TABLE_JOGADAS = "jogadas";

    // Books Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_DATA = "data";
    private static final String KEY_TEMPO = "tempo";

    private static final String[] COLUMNS = {KEY_ID,KEY_DATA,KEY_TEMPO};

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BOOK_TABLE = "CREATE TABLE jogadas ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "data TEXT, "+
                "tempo REAL )";

        db.execSQL(CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS jogadas");

        this.onCreate(db);
    }

    public void addJogada(Jogada j){

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_DATA, j.getDia()); // get title
        values.put(KEY_TEMPO, j.getTempo()); // get author

        // 3. insert
        db.insert(TABLE_JOGADAS, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }



    public List<Jogada> getMelhoresJogadas() {
        List<Jogada> jogadas = new LinkedList<Jogada>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_JOGADAS + " ORDER BY tempo DESC LIMIT 30";

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        Jogada j = null;
        if (cursor.moveToFirst()) {
            do {
                j = new Jogada();
                j.setId(Integer.parseInt(cursor.getString(0)));
                j.setDia(cursor.getString(1));
                j.setTempo(cursor.getString(2));

                // Add book to books
                jogadas.add(j);
            } while (cursor.moveToNext());
        }

        return jogadas;
    }

    public List<Jogada> getAllJogadas() {
        List<Jogada> jogadas = new LinkedList<Jogada>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_JOGADAS + " ORDER BY id DESC LIMIT 30";

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        Jogada j = null;
        if (cursor.moveToFirst()) {
            do {
                j = new Jogada();
                j.setId(Integer.parseInt(cursor.getString(0)));
                j.setDia(cursor.getString(1));
                j.setTempo(cursor.getString(2));

                // Add book to books
                jogadas.add(j);
            } while (cursor.moveToNext());
        }

       return jogadas;
    }

}