package br.com.luissiqueira.dezenovesegundos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import br.com.luissiqueira.dezenovesegundos.sqlite.MySQLiteHelper;


public class SplashActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MySQLiteHelper mdb = new MySQLiteHelper(this);

        if(mdb.getReadableDatabase().getVersion() < 2)
            mdb.onUpgrade(mdb.getWritableDatabase(), 1, 2);

        getSupportActionBar().hide();
        new Thread(){
            @Override
            public void run() {
                try {
                    sleep(4000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent it = new Intent(getBaseContext(), GameActivity.class);
                            startActivity(it);
                            finish();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }



}
